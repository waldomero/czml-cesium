# Installation
1. Run `npm install`
2. Install the _copy-from-to_ package globally by executing `npm install -g copy-files-from-to`.
3. Execute `copy-files-from-to` to copy the necessary files to a more accessible location.
